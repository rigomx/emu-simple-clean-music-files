import os
import time
import shutil

def get_file_stats(file_path):
    file_stats = os.stat(file_path)
    return {
        "file_name": os.path.basename(file_path),
        "location": os.path.dirname(file_path),
        "file_size": file_stats.st_size,
        "creation_time": time.strftime(
            "%Y-%m-%d %H:%M:%S", time.localtime(file_stats.st_ctime)
        ),
        "modification_time": time.strftime(
            "%Y-%m-%d %H:%M:%S", time.localtime(file_stats.st_mtime)
        ),
        "last_access_time": time.strftime(
            "%Y-%m-%d %H:%M:%S", time.localtime(file_stats.st_atime)
        ),
    }

def find_music_files(main_directory):
    music_files = []
    for root, _, files in os.walk(main_directory):
        for file in files:
            if file.lower().endswith((".mp3", ".wav")):
                music_files.append(os.path.join(root, file))
                print(f"Found music file: {os.path.join(root, file)}")
    return music_files

def parse_file_name(file_name):
    parts = file_name.rsplit(".", 1)[0].split("_", 2)
    if len(parts) < 3:
        return {"label": None, "artist": None, "track_name": None}
    return {"label": parts[0], "artist": parts[1], "track_name": parts[2]}

def organize_files(music_files, output_directory):
    files_dict = {}
    for file_path in music_files:
        print(f"Processing file: {file_path}")
        file_stats = get_file_stats(file_path)
        file_info = parse_file_name(file_stats["file_name"])
        label = file_info["label"] or "NO LABEL"
        artist = file_info["artist"] or "NO ARTIST"
        unique_key = f"{label}_{artist}_{file_info['track_name']}"

        if unique_key in files_dict:
            if file_stats["file_size"] > files_dict[unique_key]["file_size"]:
                duplicates_dir = os.path.join(output_directory, "DUPLICATES")
                os.makedirs(duplicates_dir, exist_ok=True)
                existing_file_path = files_dict[unique_key]["file_path"]
                shutil.copy(existing_file_path, duplicates_dir)
                print(f"Moved duplicate to {duplicates_dir}: {existing_file_path}")
                files_dict[unique_key] = {
                    "file_size": file_stats["file_size"],
                    "file_path": file_path,
                }
        else:
            files_dict[unique_key] = {
                "file_size": file_stats["file_size"],
                "file_path": file_path,
            }

    for unique_key, file_info in files_dict.items():
        label, artist, _ = unique_key.split("_", 2)
        target_dir = os.path.join(output_directory, label, artist)
        os.makedirs(target_dir, exist_ok=True)
        target_file_path = os.path.join(
            target_dir, os.path.basename(file_info["file_path"])
        )
        if not os.path.exists(target_file_path):
            shutil.copy(file_info["file_path"], target_dir)
            print(f"Copied file to {target_dir}: {file_info['file_path']}")
        else:
            print(f"File already exists: {target_file_path}")

def delete_json_files(directory):
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith(".json"):
                file_path = os.path.join(root, file)
                os.remove(file_path)
                print(f"Deleted JSON file: {file_path}")

main_directory = "./files"
music_files = find_music_files(main_directory)

output_directory = "./output"
print("Iniciando organización de archivos...")
organize_files(music_files, output_directory)
print("Organización de archivos completa.")

print("Iniciando eliminación de archivos JSON...")
delete_json_files(main_directory)
print("Eliminación de archivos JSON completa.")

print("Done!")
