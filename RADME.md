# Organizador de archivos de música

Este es un script de Python que organiza archivos de música en una estructura de directorios basada en la información de metadatos de los archivos. El script busca archivos de música en un directorio principal y los organiza en subdirectorios según la etiqueta de la discográfica, el artista y el nombre de la pista. Si hay archivos duplicados, el script conserva el archivo más grande y mueve los demás a un directorio de duplicados. 

## Requisitos

- Python 3.x
- mutagen

## Uso

1. Coloque los archivos de música en el directorio `./files`.
2. Ejecute el script `main.py`.
3. Los archivos organizados se colocarán en el directorio `./output`.

## Notas

- El script solo admite archivos de música con extensiones `.mp3` y `.wav`.
- Si un archivo de música no tiene información de metadatos, se colocará en un directorio `NO LABEL/NO ARTIST`.
- Si hay varios archivos de música con el mismo nombre de pista, el script conserva el archivo más grande y mueve los demás a un directorio de duplicados.
- El script elimina todos los archivos JSON generados en el directorio de original, independientemente de su contenido.
